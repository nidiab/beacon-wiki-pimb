# Welcome to PIMB Wiki

#### Project Inter-hospitalaria Marató Beacon Wiki.

Aqui posarem instruccións i enllaços interessants / utils,<br/>
que ens ajudara a gestionar el nostre network de beacons inter-hospitalaria.

Enquesta tècnic: [ENQUESTA.md](ENQUESTA.md)

Enllaços utils:

- Documentació oficial:

  - [https://b2ri-documentation.readthedocs.io/en/latest/](https://b2ri-documentation.readthedocs.io/en/latest/)
  - [https://b2ri-documentation.readthedocs.io/en/latest/download-and-installation/](https://b2ri-documentation.readthedocs.io/en/latest/download-and-installation/)

- Software:

  - beacon-ri-specific:

    - https://github.com/EGA-archive/beacon2-ri-tools
    - https://github.com/EGA-archive/beacon2-ri-api

  - data/format:

    - https://www.visidata.org/
    - https://stedolan.github.io/jq/manual/
    - https://jless.io/
    - https://miller.readthedocs.io/en/latest/
    - https://mikefarah.gitbook.io/yq/

  - Apps:
    
    - Percona MongoDB:
       
        - Ansible installer: 
            - url: https://github.com/joe-opensrc/ansible-percona-mongodb
            - desc: Used to install mongodb onto SLES/opensuse & Debian.

- Dades sintètiques:

  - https://gitlab.com/xisc/beacon-wiki-pimb/dades/CINECA
